import fs from 'fs';
import path from 'path';
import glob from 'glob';

function generateCacheManifest ( options ) {
	return {
		resolveId ( id ) {
			if ( id === options.id ) return id;
		},

		load ( id ) {
			if ( id === options.id ) {
				const files = glob.sync( options.pattern || '**', { cwd: options.dir })
					.map( file => `/${file}` )
					.concat( '/' );

				return `export default ${JSON.stringify(files)}`;
			}
		}
	}
}

export default {
	entry: 'src/service-worker.js',
	dest: 'dist/service-worker.js',
	format: 'es',
	plugins: [
		// the \0 is a plugin convention, prevents virtual modules
		// being confused with stuff on the filesystem
		generateCacheManifest({
			id: '\0cache-manifest',
			dir: 'static'
		})
	]
}
