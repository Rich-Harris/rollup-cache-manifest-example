import filesToCache from '\0cache-manifest';

console.log( `filesToCache`, filesToCache )

self.addEventListener( 'install', event => {
	event.waitUntil(
		caches.open( 'v1' ).then( cache => cache.addAll( filesToCache ) )
	);

	self.skipWaiting();
});

self.addEventListener( 'activate', event => {
	self.clients.claim();
});

self.addEventListener( 'fetch', event => {
	event.respondWith(
		caches.match( event.request ).then( response => {
			return response || fetch( event.request );
		})
	);
});
